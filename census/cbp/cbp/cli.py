import argparse

from dotenv import load_dotenv

from cbp.Downloader import download_cbp_data
from cbp.Preprocess import preprocess_cbp_data
from cbp.Loader.Load import load_cbp_data


def make_parser() -> argparse.ArgumentParser:
    """Create the CLI parser for this script."""
    parser = argparse.ArgumentParser(
        description="Handle downloading and loading Census County Business Pattern data"
    )
    parser.add_argument(
        "-d",
        "--download",
        action="store_true",
        help="Download the data from the Census website",
    )
    parser.add_argument(
        "-o",
        "--overwrite-existing",
        action="store_true",
        help="Overwrite existing files when downloading to disk",
    )
    parser.add_argument(
        "-p",
        "--preprocess",
        action="store_true",
        help="Preprocess the downloaded CBP data",
    )
    parser.add_argument(
        "-l",
        "--load",
        action="store_true",
        help="Load the preprocessed data into the database",
    )
    return parser


def main():
    load_dotenv()
    parser = make_parser()
    args = parser.parse_args()
    if args.download:
        args.overwrite_existing
        download_cbp_data(overwrite_existing=args.overwrite_existing)
    if args.preprocess:
        preprocess_cbp_data()
    if args.load:
        load_cbp_data()
