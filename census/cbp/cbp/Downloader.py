import os
import re
import requests
import logging
import pathlib

from typing import List
from bs4 import BeautifulSoup


def validate_env() -> None:
    """Checks that the environment variables required to run
    the downloader are set.

    Raises:
        KeyError: The environment variable is not set
    """
    required_env_vars = [
        "CENSUS_CBP_DOWNLOADER_CBP_DATASETS_URL",
        "CENSUS_CBP_DOWNLOADER_DOWNLOAD_FILE_REGEX",
        "CENSUS_CBP_DATA_DIR",
    ]

    for env_var in required_env_vars:
        if not os.environ.get(env_var):
            raise KeyError(f"{env_var} environment variable is not set")


def get_html_from_url(url: str) -> str:
    """Returns the HTML contents of a URL

    Args:
        url (str): The URL to get the HTML from

    Returns:
        str: The HTML contents of the URL
    """
    response = requests.get(url)
    return response.text


def get_cbp_dataset_links() -> List[str]:
    """Gets a list of links to pages for CBP dataset years from the CBP
    dataset URL from the environment variable:
        CENSUS_CBP_DOWNLOADER_CBP_DATASETS_URL

    Returns:
        List[str]: The list of links to the yearly CBP dataset pages
    """
    cbp_datasets_url = os.environ.get(
        "CENSUS_CBP_DOWNLOADER_CBP_DATASETS_URL", default=""
    )
    html = get_html_from_url(cbp_datasets_url)
    soup = BeautifulSoup(html, "html.parser")
    links = []
    for link in soup.find_all("a"):
        ref = link.get("href")
        if not ref:
            continue
        if "data/datasets/" in link.get("href"):
            links.append(ref)

    # links don't include the base URL for these so we need to add it
    links = ["https://www.census.gov" + link for link in links]
    return links


def get_file_links_from_cbp_dataset(
    dataset_url: str,
) -> List[str]:
    """Given a URL to a yearly CBP dataset page, returns a list of links
    to files that should be downloaded.

    Files included must match the regex specified in the environment
    variable: CENSUS_CBP_DOWNLOADER_DOWNLOAD_FILE_REGEX

    Args:
        dataset_url (str): The URL to the yearly CBP dataset page
            Ex: https://www.census.gov/data/datasets/2020/econ/cbp/2020-cbp.html

    Returns:
        List[str]: A list of URLs of files to download
    """
    file_regex = os.environ.get("CENSUS_CBP_DOWNLOADER_DOWNLOAD_FILE_REGEX", default="")
    html = get_html_from_url(dataset_url)
    soup = BeautifulSoup(html, "html.parser")

    links = []
    for link in soup.find_all("a"):
        ref = link.get("href")
        if not ref:
            continue
        if re.search(file_regex, ref):
            links.append(ref)

    return links


def get_all_cbp_file_links(dataset_links: List[str]) -> List[str]:
    """Given a list of links to annual CBP dataset pages, returns a list
    of links to all the CBP files to download.

    Args:
        dataset_links (List[str]): URLs to CBP dataset pages

    Returns:
        List[str]: URLs of CBP files to download
    """
    links = []
    for link in dataset_links:
        links.extend(get_file_links_from_cbp_dataset(link))
    links = list(set(links))  # duplicates exist in reference files
    logging.info(f"Found {len(links)} total file links to download")
    return links


def download_file(url: str) -> bytes:
    """Downloads the contents of a URL as bytes

    Args:
        url (str): The URL to download the bytes from

    Returns:
        bytes: The contents of the URL as bytes
    """
    logging.info(f"Downloading content from {url}")
    response = requests.get(url)
    return response.content


def write_bytes_to_disk(data: bytes, file_path: str) -> None:
    """Takes a bytes and writes it to a file on disk
    If the path to the file does not exist, necessary
    folders will be created.

    Args:
        data (bytes): The bytes to write to disk
        file_path (str): The path to the file to write to
    """
    path = pathlib.Path(file_path)
    path.parent.mkdir(parents=True, exist_ok=True)
    path.write_bytes(data)


def download_file_to_disk(url: str, file_path: str) -> None:
    """Downloads a file given a URL to disk at the given path

    Args:
        url (str): The URL to download from
        file_path (str): The path to save the file to
    """
    file_bytes = download_file(url)
    write_bytes_to_disk(file_bytes, file_path)


def download_files_to_disk(urls: List[str], overwrite_existing: bool) -> None:
    """Downloads all files from a list to disk.

    Args:
        urls (List[str]): The list of URLs to download
        overwrite_existing (bool): Whether or not to overwrite files that already exist
    """
    data_dir = os.environ.get("CENSUS_CBP_DATA_DIR", default="")
    for url in urls:
        file_path = url.split("/cbp/", maxsplit=1)[1]
        file_path = f"{data_dir}/{file_path}"
        # clean up spaces and trailing ?#s
        file_path = file_path.replace("%20", "-").replace(" ", "-").replace("?#", "")
        if pathlib.Path(file_path).exists() and not overwrite_existing:
            logging.info(f"Skipping {file_path} because it already exists")
            continue
        else:
            download_file_to_disk(url, file_path)


def clean_download_link(url: str) -> str:
    """Cleans up links to files by fixing problems I've encountered
    with links from CBP webpages.

    Args:
        url (str): The URL to clean

    Returns:
        str: The cleaned up URL
    """
    url = url.split("www", maxsplit=1)[1]
    url = "https://www" + url
    return url


def clean_download_links(urls: List[str]) -> List[str]:
    """Cleans a list of URLs

    Args:
        urls (List[str]): The list of URLs to clean

    Returns:
        List[str]: The cleaned up URLs
    """
    return [clean_download_link(url) for url in urls]


def download_cbp_data(overwrite_existing: bool) -> None:
    """Downloads all CBP data to disk.

    Args:
        overwrite_existing (bool): Whether or not to overwrite files that
        already exist on disk.
    """
    validate_env()
    links = get_cbp_dataset_links()
    links = get_all_cbp_file_links(links)
    links = clean_download_links(links)
    download_files_to_disk(urls=links, overwrite_existing=overwrite_existing)
    logging.info("Successfully downloaded all CBP data.")
