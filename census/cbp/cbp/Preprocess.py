import os
import glob
import logging
import zipfile


def validate_env() -> None:
    """Checks that all environment variables required for
    preprocessing have been set.

    Raises:
        KeyError: The environment variable is not set.
    """
    required_env_vars = [
        "CENSUS_CBP_DATA_DIR",
    ]
    for env_var in required_env_vars:
        if not os.environ.get(env_var):
            raise KeyError(f"{env_var} environment variable is not set")


def unzip_file(file_path: str) -> None:
    """Unzips a file at a given path.

    Args:
        file_path (str): The path to the file to unzip
    """
    logging.info(f"Unzipping {file_path}")
    with zipfile.ZipFile(file_path, "r") as zip_ref:
        zip_ref.extractall(os.path.dirname(file_path))


def unzip_zip_files() -> None:
    """Unzips all zip files found in the data directory.

    The data directory is defined by the environment variable:
        CENSUS_CBP_DATA_DIR
    """
    data_dir = os.environ.get("CENSUS_CBP_DATA_DIR", default="")
    zip_files = glob.glob(f"{data_dir}/**/*.zip", recursive=True)
    for zip_file in zip_files:
        unzip_file(zip_file)


def preprocess_cbp_data():
    validate_env()
    unzip_zip_files()
