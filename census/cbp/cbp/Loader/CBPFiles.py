import os
import glob
import logging
import pandas as pd
import sqlalchemy

from typing import List, Dict


class CBPFileLoader:
    """Handles loading the CBP files into a database."""

    FILE_TYPES_TO_LOAD = [
        "zbptotals",
        "zbpdetail",
        "cbpus",
        "cbpco",
        "cbpmsa",
        "cbpst",
    ]

    def __init__(self) -> None:
        self.engine = sqlalchemy.create_engine(
            os.environ.get("CENSUS_CBP_DB_CONNECTION_STRING")
        )

    def _get_files(self, data_dir: str) -> List[str]:
        """Returns a list of all the files we want to load
        in the data directory

        Args:
            data_dir (str): The directory our CBP files are in.

        Returns:
            List[str]: The paths of the files we want to load.
        """
        cbp_files = glob.glob(
            f"{data_dir}/datasets/**/?bp[0-9][0-9]*.txt", recursive=True
        )
        return cbp_files

    def _get_cbp_file_type(self, file_path: str) -> str:
        """Returns the "type" of CBP file given a file path.
        Ex: zbp03detail.txt is "zbpdetail"

        Args:
            file_path (str): The path to the file.

        Returns:
            str: The type of CBP file.
        """
        file_type = file_path.split("/")[-1].split(".")[0]
        file_type = file_type[:3] + file_type[5:]
        file_type = file_type.lower()
        return file_type

    def _get_files_by_type(self, files: List[str]) -> Dict[str, List[str]]:
        """Given a list of file paths, returns a dictionary of the form:
        {
            "file_type": [file_paths], ...
        }

        Args:
            files (List[str]): A list of file paths.

        Returns:
            Dict[str, List[str]]: A dictionary with keys of file types and
            values of lists of file paths.
        """
        files_by_type: Dict[str, List[str]] = {}
        for file in files:
            file_type = self._get_cbp_file_type(file)
            if file_type not in files_by_type:
                files_by_type[file_type] = []
            files_by_type[file_type].append(file)
        return files_by_type

    def _clean_df_col_names(self, df: pd.DataFrame) -> None:
        """Cleans the column names of input dataframes in-place.

        Args:
            df (pd.DataFrame): The dataframe whose column names we want to clean.
        """
        df.columns = df.columns.str.lower()
        df.columns = df.columns.str.replace("<5", "1_4")

    def _get_all_col_names_from_files(self, files: List[str]) -> List[str]:
        """Returns a list of all the column names in a list of files.

        Args:
            files (List[str]): The list of files whose column names we want.

        Returns:
            List[str]: The list of column names.
        """
        cols = []
        # to keep column names in the correct order, we do this inelegantly
        for file in files:
            df = pd.read_csv(file, encoding="latin-1", nrows=1, dtype=str)
            self._clean_df_col_names(df)
            for col in df.columns:
                if col not in cols:
                    cols.append(col)
        return cols

    def _create_table_for_type(self, files: List[str], file_type: str) -> None:
        """Creates the table for a given file type in the database.

        Args:
            files (List[str]): The list of files for the given file type.
            file_type (str): The file type we want to create a table for.
        """
        cols = ["year"] + self._get_all_col_names_from_files(files)
        conn = self.engine.connect()
        query = f"""
        CREATE SCHEMA IF NOT EXISTS cbp;
        DROP TABLE IF EXISTS cbp.{file_type};
        CREATE TABLE cbp.{file_type} (
            {', '.join([f'{col} text' for col in cols])}
        );
        """
        conn.execute(query)
        conn.close()

    def _load_file(self, file: str, file_type: str) -> None:
        """Loads a single file of a given type into the database.

        Args:
            file (str): The path to the file to load.
            file_type (str): The type of file we want to load.
        """
        logging.info(f"Loading {file}")
        year = int(file.split("/")[-2])
        df = pd.read_csv(file, encoding="latin-1", dtype=str)
        df["year"] = year
        self._clean_df_col_names(df)
        df.to_sql(file_type, self.engine, "cbp", if_exists="append", index=False)

    def _load_files_for_type(self, files: List[str], file_type: str) -> None:
        """Loads all the files for a given file type.

        Args:
            files (List[str]): The list of files for the given file type.
            file_type (str): The file type we want to load.
        """
        self._create_table_for_type(files, file_type)
        for file in files:
            self._load_file(file, file_type)

    def load_files(self) -> None:
        """Loads all the loadable files in the data directory."""
        data_dir = os.environ.get("CENSUS_CBP_DATA_DIR", default="")
        files = self._get_files(data_dir)
        files_by_type = self._get_files_by_type(files)
        for file_type in self.FILE_TYPES_TO_LOAD:
            self._load_files_for_type(files_by_type[file_type], file_type)
