import os
from cbp.Loader.CBPFiles import CBPFileLoader


def validate_env() -> None:
    """Checks that all environment variables required for
    loading the data have been set.

    Raises:
        KeyError: The environment variable is not set.
    """

    required_env_vars = [
        "CENSUS_CBP_DATA_DIR",
        "CENSUS_CBP_DB_CONNECTION_STRING",
    ]
    for env_var in required_env_vars:
        if not os.environ.get(env_var):
            raise KeyError(f"{env_var} environment variable is not set")


def load_cbp_data():
    validate_env()
    loader = CBPFileLoader()
    loader.load_files()
