# County Business Patterns (CBP)

The CBP pipeline will load the raw data from the [Census County Business Patterns](https://www.census.gov/programs-surveys/cbp.html) into the configured database.

Data is annual and goes back to 1986 for some tables.

The tables created by the CBP pipeline are:

| Table         | Geographic Granularity  | Industry Granularity
|---------------|-------------------------|-----------------------
| cbp.cbpus     | Country (US)            | NAICS
| cbp.cbpst     | State                   | NAICS
| cbp.cbpmsa    | MSA                     | NAICS
| cbp.zbpdetail | Zip Code                | NAICS
| cbp.zbptotals | Zip Code                | Totals

## Installation

Simply run the following commands to install the CBP library:

```shell
git clone https://gitlab.com/thevillagers/goatrealestate/data-pipelines.git
cd data-pipelines/census/cbp
poetry install
```

## Usage

**From [Configuration](#configuration) section, be sure to set CENSUS_CBP_DB_CONNECTION_STRING env var before running the load step**

To run the entire CBP loading pipeline, you run

```shell
poetry run cbp_cli -dpl
```

Full CLI usage reference (`poetry run cbp_cli -h`):

```lang-plaintext
usage: cbp_cli [-h] [-d] [-o] [-p] [-l]

Handle downloading and loading Census County Business Pattern data

options:
  -h, --help            show this help message and exit
  -d, --download        Download the data from the Census website
  -o, --overwrite-existing
                        Overwrite existing files when downloading to disk
  -p, --preprocess      Preprocess the downloaded CBP data
  -l, --load            Load the preprocessed data into the database
```

## Configuration

The `.env` file holds environment variables required by the CBP pipeline.

Various variables will need to be configured to match your environment.

Others may need to change to accomodate your preferences.

### DB Connection String

`CENSUS_CBP_DB_CONNECTION_STRING`

First & most important, this one is absent from the default `.env` file. I obviously don't know your database credentials. So set them in your env before running the load step. You can throw it in `.env` or just export it to your shell.

### Data Directory

`CENSUS_CBP_DATA_DIR`

Determines where the CBP data will be downloaded to, relative to the root directory. Defaults to `data` in the `.env` file.

### Download File Regex

`CENSUS_CBP_DOWNLOADER_DOWNLOAD_FILE_REGEX`

Specifies a regular expression used to determine which files should be downloaded from the Census website. Defaults to `(\.csv|\.xlsx|\.xls|\.txt|\.zip)` in the `.env` file.

### CBP URL

`CENSUS_CBP_DOWNLOADER_CBP_DATASETS_URL`

Holds the URL for the CBP data homepage. It hasn't changed in the couple years I've been looking at it, but just in case. Defaults to <https://www.census.gov/programs-surveys/cbp/data/datasets.html> in the `.env` file.
