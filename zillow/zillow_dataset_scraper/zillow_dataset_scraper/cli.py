import argparse

from dotenv import load_dotenv

from zillow_dataset_scraper import _validate_env
from zillow_dataset_scraper.Download import download_zillow_csvs, clean_data_dir
from zillow_dataset_scraper.Preprocess import preprocess_zillow_csvs
from zillow_dataset_scraper.Load import load_zillow_datasets


def make_parser() -> argparse.ArgumentParser:
    """Create the CLI parser for this script."""
    parser = argparse.ArgumentParser(
        description="Handle downloading and loading Census County Business Pattern data"
    )
    parser.add_argument(
        "-d",
        "--download",
        action="store_true",
        help="Download the CSVs from Zillow",
    )
    parser.add_argument(
        "-p",
        "--preprocess",
        action="store_true",
        help="Preprocess the downloaded data",
    )
    parser.add_argument(
        "-l",
        "--load",
        action="store_true",
        help="Load the preprocessed data into the database",
    )
    parser.add_argument(
        "-c",
        "--clean",
        action="store_true",
        help="Clean the data directory of old files",
    )
    return parser


def main():
    load_dotenv()
    parser = make_parser()
    args = parser.parse_args()
    if args.clean:
        _validate_env()
        clean_data_dir()
    if args.download:
        _validate_env()
        download_zillow_csvs()
    if args.preprocess:
        _validate_env()
        preprocess_zillow_csvs()
    if args.load:
        _validate_env()
        load_zillow_datasets()
