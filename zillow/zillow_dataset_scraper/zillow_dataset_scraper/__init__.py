import os
import logging

logging.basicConfig(
    level=logging.INFO,
    format="%(asctime)s - %(name)s - %(levelname)s - %(message)s",
)

__version__ = "0.1.0"


def _get_data_dir() -> str:
    """Returns the data directory from the environment variable

    Returns:
        str: The data directory
    """
    return os.environ.get("ZILLOW_PUBLIC_DATA_DIR", default="")


def _get_db_conn_str() -> str:
    """Returns the database connection string from the environment variable

    Returns:
        str: The database connection string
    """
    return os.environ.get("ZILLOW_PUBLIC_DB_CONNECTION_STRING", default="")


def _validate_env():
    """Validates the environment variables.

    Raises:
        KeyError: The environment variable is not set.
    """
    required_env_vars = ["ZILLOW_PUBLIC_DATA_DIR", "ZILLOW_PUBLIC_DB_CONNECTION_STRING"]
    for env_var in required_env_vars:
        if not os.environ.get(env_var):
            raise KeyError(f"{env_var} is not set")
