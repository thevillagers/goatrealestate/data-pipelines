import os
import re
import logging
import json
import requests
import shutil
import pathlib

from bs4 import BeautifulSoup
from typing import List, Dict, Any, Tuple
from zillow_dataset_scraper import _get_data_dir


def get_html(url):
    """Returns the HTML of a URL.

    Args:
        url (str): URL to get HTML from.

    Returns:
        str: HTML of the URL.
    """
    response = requests.get(url)
    return response.text


def download_file_from_url(url: str) -> bytes:
    """Downloads a file from a URL and returns the contents

    Args:
        url (str): The URL to download the file from

    Returns:
        bytes: The content of the file
    """
    logging.info(f"Downloading {url}")
    resp = requests.get(url)
    return resp.content


def get_csv_links() -> Dict[str, Any]:
    """Gets the links to all CSV files on the Zillow research data page

    Raises:
        LookupError: Unable to find the links to the CSV files

    Returns:
        Dict[str, Any]: A nested dictionary where the final value is
        the URL of a CSV file. The keys describe the type of data.
    """
    html = get_html("https://www.zillow.com/research/data/")
    soup = BeautifulSoup(html, "html.parser")
    found = False
    for script in soup.find_all("script"):
        if "var data = " in script.text:
            found = True
            break
    if not found:
        raise LookupError("Could not find data in HTML.")
    data = script.text.split("var data = ")[1].split(";")[0]
    data = json.loads(data)
    return data


def get_download_paths(
    csv_data: Dict[str, Any], parent_path: str = ""
) -> List[Tuple[str, str]]:
    """Recursively goes through the dictionary to generate a folder path to
    download each CSV link to.

    Args:
        csv_data (Dict[str, Any]): The dictionary containing CSVs as the value leafs.
        parent_path (str, optional): The dictionary keys we've already been to,
        formatted as a folder path. Defaults to "".

    Returns:
        List[Tuple[str, str]]: A list of tuples where the first value is the
        folder path and the second value is the URL to download the CSV from.
    """
    paths = []
    for key, value in csv_data.items():
        if isinstance(value, dict):
            paths.extend(get_download_paths(value, f"{parent_path}{key}/"))
        else:
            paths.append((f"{parent_path}{key}/", value))
    return paths


def clean_folder_path(path: str) -> str:
    """Cleans a folder path

    Args:
        path (str): The path to clean

    Returns:
        str: The cleaned path
    """
    path = path.lower()
    path = re.sub(r"[^0-9a-z\/]", " ", path)
    path = re.sub(r"\s+", " ", path)
    path = re.sub(r"_+\/", "/", path)
    path = path.strip()
    path = path.replace(" ", "_")
    return path


def clean_url(url: str) -> str:
    """Cleans the download URL for our CSVs.

    Args:
        url (str): The CSV url

    Returns:
        str: The cleaned up URL
    """
    url = url.split("?")[0]
    return url


def clean_download_path(path: Tuple[str, str]) -> Tuple[str, str]:
    folder_path, url = path
    folder_path = clean_folder_path(folder_path)
    url = clean_url(url)
    return folder_path, url


def clean_download_paths(paths: List[Tuple[str, str]]) -> List[Tuple[str, str]]:
    """Cleans the download paths to remove the unnecessary parts"""
    paths = [clean_download_path(path) for path in paths]
    return paths


def download_url_to_file(url: str, folder_path: str) -> None:
    """Downloads a file from a URL and saves it to a file

    Args:
        url (str): The URL to download the file from
        path (str): The path to save the file to
    """
    data_dir = _get_data_dir()
    path = pathlib.Path(folder_path)
    path = data_dir / path
    file_name = url.split("/")[-1]
    file_path = path / file_name
    path.mkdir(parents=True, exist_ok=True)
    file = download_file_from_url(url)
    with open(file_path, "wb") as f:
        f.write(file)


def download_csvs(csv_data: Dict[str, Any]) -> None:
    """Downloads all the CSVs from the Zillow research data page

    Args:
        csv_data (Dict[str, Any]): The dictionary containing CSVs as the value leafs.
    """
    paths = get_download_paths(csv_data)
    paths = clean_download_paths(paths)
    for folder_path, url in paths:
        download_url_to_file(url, folder_path)


def download_zillow_csvs() -> None:
    """Downloads all the CSVs from Zillow."""
    csv_data = get_csv_links()
    download_csvs(csv_data)


def clean_data_dir() -> None:
    """Cleans the data directory"""
    data_dir = _get_data_dir()
    path = pathlib.Path(data_dir)
    shutil.rmtree(path)
    path.mkdir(parents=True, exist_ok=True)
