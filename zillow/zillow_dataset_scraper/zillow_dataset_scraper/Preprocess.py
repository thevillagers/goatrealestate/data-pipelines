import glob
import pathlib
import shutil

from typing import List

from zillow_dataset_scraper import _get_data_dir
from zillow_dataset_scraper.Renamer import ZillowFileRenamer


def rename_csv_file(file: str) -> None:
    """Renames a given CSV file using the ZillowFileRenamer

    Args:
        file (str): The path to the file to rename
    """
    zillow_file_renamer = ZillowFileRenamer()
    file_name = pathlib.Path(file).name
    file_folder = pathlib.Path(file).parent
    file_name = file_name.split(".csv")[0]
    new_file_name = zillow_file_renamer.rename(file_name)
    new_file_name = f"{new_file_name}.csv"
    new_file_path = file_folder / new_file_name
    shutil.move(file, str(new_file_path))


def rename_csv_files(files: List[str]) -> None:
    """Renames a list of CSV files

    Args:
        files (List[str]): The list of file paths to rename
    """
    for file in files:
        rename_csv_file(file)


def preprocess_zillow_csvs() -> None:
    """Preprocess the downloaded data by renaming
    the files to a more reliable format.
    """
    data_dir = _get_data_dir()
    files = glob.glob(f"{data_dir}/**/*.csv", recursive=True)
    rename_csv_files(files)
