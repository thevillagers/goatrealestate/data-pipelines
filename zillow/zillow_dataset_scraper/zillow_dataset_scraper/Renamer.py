import re
import logging


class ZillowFileRenamer:

    FILE_NAME_MAP = {
        "metro": "mtro",
        "state": "stat",
        "county": "cnty",
        "neighborhood": "nbhd",
        "sfrcondo": "allhomes",
        "allhomesplusmultifamily": "allhomes",
        "bdrmcnt_1": "1br",
        "bdrmcnt_2": "2br",
        "bdrmcnt_3": "3br",
        "bdrmcnt_4": "4br",
        "bdrmcnt_5": "5br",
        "uc": "",
        "median_sale_price": "medsp",
        "listings_price": "lp",
        "smoothed": "sm",
        "sm_sa": "ssa",
        "tier_0.33_0.67": "midtier",
        "tier_0.0_0.33": "lotier",
        "tier_0.67_1.0": "hitier",
        "mean_doz_pending": "mean2pnd",
        "med_doz_pending": "med2pnd",
        "perc_listings_price_cut": "plpc",
        "pending": "pnd",
        "median": "med",
    }

    def rename(self, file_name: str, split_char: str = "_") -> str:
        """Renames a file name according to the mapping

        Args:
            file_name (str): The file name to rename
            split_char (str): The character to split the name on

        Returns:
            str: The renamed file name
        """
        file_name = file_name.lower()
        split_name = file_name.split(split_char)
        combos = []
        for start_pos, start_word in enumerate(split_name):
            word_builder = start_word
            combos.append((word_builder, start_pos, 1))
            for curr_pos, new_word in enumerate(split_name[start_pos + 1 :]):
                word_builder = f"{word_builder}{split_char}{new_word}"
                combos.append((word_builder, start_pos, curr_pos + 2))

        regex_matches = []
        for combo in combos:
            for match_str, replace_str in self.FILE_NAME_MAP.items():
                if match_str == combo[0]:
                    regex_matches.append(combo + (replace_str,))

        regex_matches = sorted(regex_matches, key=lambda x: (x[1], x[2]))

        split_name_len = len(split_name)
        split_accounted_for = [False] * split_name_len

        full_word = ""
        full_word_parts = []
        for regex_match in regex_matches:
            match_start_pos = regex_match[1]
            match_len = regex_match[2]
            if not split_accounted_for[regex_match[1]]:
                split_accounted_for[match_start_pos : match_start_pos + match_len] = [
                    True for _ in range(match_len)
                ]
                regex_word = regex_match[3]
                if not regex_word:
                    continue
                full_word = f"{full_word}{regex_word}{split_char}"
                full_word_parts.append(regex_match)
        full_word_pieces = []
        for i, split_accounted_for_i in enumerate(split_accounted_for):
            if not split_accounted_for_i:
                full_word_pieces.append(split_name[i])
            else:
                for regex_match in full_word_parts:
                    if regex_match[1] == i:
                        full_word_pieces.append(regex_match[3])
        full_word = split_char.join(full_word_pieces)
        logging.info(f"Mapped name {file_name} to {full_word}")
        return full_word
