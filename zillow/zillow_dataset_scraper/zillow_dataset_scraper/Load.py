import os
import glob
import logging
import pathlib
import pandas as pd
import sqlalchemy

from typing import List, Dict

from zillow_dataset_scraper import _get_data_dir, _get_db_conn_str


def create_zillow_schema(engine: sqlalchemy.engine.Engine) -> None:
    """Creates the schema for the Zillow datasets"""
    engine.execute("CREATE SCHEMA IF NOT EXISTS zillow;")


def clean_column_names(df: pd.DataFrame) -> None:
    """Cleans the column names of a dataframe.

    Args:
        df (pd.DataFrame): The dataframe to clean.
    """
    df.columns = df.columns.str.lower()


def load_dataset_to_db(file: str, engine: sqlalchemy.engine.Engine) -> None:
    """Loads a single dataset into the database.

    Args:
        file (str): The path to the file to load.
    """
    logging.info(f"Loading {file}")
    table_name = pathlib.Path(file).stem
    df = pd.read_csv(file, dtype=str)
    clean_column_names(df)
    df.to_sql(table_name, engine, "zillow", if_exists="replace", index=False)


def load_datasets_to_db(files: List[str]) -> None:
    """Loads the list of files into the database.

    Args:
        files (List[str]): List of file paths.
    """
    engine = sqlalchemy.create_engine(_get_db_conn_str())
    create_zillow_schema(engine)
    for file in files:
        load_dataset_to_db(file, engine)


def load_zillow_datasets() -> None:
    """Loads all datasets into the database"""
    data_dir = _get_data_dir()
    files = glob.glob(f"{data_dir}/**/*.csv", recursive=True)
    load_datasets_to_db(files)
